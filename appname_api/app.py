from appname_core.appname_core.models import Cat
from flask import Flask

app = Flask(__name__)


@app.route('/')
def hello_world():
    c = Cat()
    return c.meow()

if __name__ == '__main__':
    app.run()
